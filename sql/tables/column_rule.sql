CREATE TABLE themask.column_rule (
  context_name VARCHAR(20)            NOT NULL,
  schema_name  VARCHAR(63)            NOT NULL,
  table_name   VARCHAR(63)            NOT NULL,
  column_name  VARCHAR(63)            NOT NULL,
  operation    themask.MASK_OPERATION NOT NULL,
  args         JSONB                  NOT NULL,
  FOREIGN KEY (context_name, schema_name, table_name)
  REFERENCES themask.table_policy (context_name, schema_name, table_name),
  PRIMARY KEY (context_name, schema_name, table_name, column_name)
);
