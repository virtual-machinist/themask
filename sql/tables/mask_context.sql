CREATE TABLE themask.mask_context (
  name                            VARCHAR(20) PRIMARY KEY,
  masked_schema_prefix            VARCHAR(10) NOT NULL,
  make_temporary_luts             BOOLEAN     NOT NULL,
  make_unlogged_luts              BOOLEAN     NOT NULL,
  make_unlogged_tables            BOOLEAN     NOT NULL,
  lut_suffix                      VARCHAR(10) NOT NULL,
  fail_on_pkey_fkey_checks        BOOLEAN     NOT NULL,
  truncate_existing_masked_tables BOOLEAN     NOT NULL,
  CONSTRAINT either_temp_or_unlogged_lut CHECK ((NOT make_temporary_luts) OR (NOT make_unlogged_luts))
);
