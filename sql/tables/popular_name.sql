CREATE TABLE themask.popular_name (
  name_type VARCHAR(1)  NOT NULL,
  rank      INT         NOT NULL,
  name      VARCHAR(25) NOT NULL,
  PRIMARY KEY (name_type, rank, name),
  CHECK (name_type IN ('F', 'L', 'M'))
);
