CREATE VIEW themask.effective_operations AS
  SELECT
    context_name,
    schema_name,
    table_name,
    column_name,
    operation,
    args
  FROM themask._default_operations;
