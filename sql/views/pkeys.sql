CREATE VIEW themask._pkeys AS
  WITH all_pkeys AS (
      SELECT
        conrelid  AS table_oid,
        a.attname AS pkey_column
      FROM
        pg_attribute a,
        (
          SELECT
            conrelid,
            conkey
          FROM themask._con_unfolded
          WHERE contype = 'p') constraints
      WHERE a.attnum = conkey AND a.attrelid = conrelid
  ) SELECT
      tbl.schema_name,
      tbl.table_name,
      apk.pkey_column
    FROM
      all_pkeys apk, themask._tables tbl
    WHERE apk.table_oid = tbl.table_oid;
