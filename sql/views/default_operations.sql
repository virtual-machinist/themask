CREATE VIEW themask._default_operations AS
  SELECT
    mtp.context_name,
    mtp.schema_name,
    mtp.table_name,
    mcd.column_name,
    CASE WHEN mcr.operation IS NOT NULL
      THEN mcr.operation
    WHEN mcr.operation IS NULL AND mtp.default_column_copy_mode = 'as_is'
      THEN 'copy' :: themask.MASK_OPERATION
    ELSE
      -- leave a default operation
      'nullify' :: themask.MASK_OPERATION
    --     WHEN mcr.operation IS NULL AND mtr.default_column_copy_mode = 'dont'
    --       THEN 'nullify' :: MASK_OPERATION
    END                   operation,
    coalesce(
      CASE WHEN mcr.args IS NOT NULL AND mcr.args <> '{}' :: JSONB
        THEN mcr.args
      WHEN mcr.operation = 'noise'
        THEN (
          CASE
          WHEN mcd.column_simple_type IN ('int2', 'int4', 'int8', 'numeric', 'float4', 'float8')
            THEN
              jsonb_build_object('fraction', mtp.default_numeric_noise_fraction)
          WHEN mcd.column_simple_type IN ('timestamp', 'timestamptz')
            THEN
              jsonb_build_object('seconds', mtp.default_timestamp_noise_seconds)
          WHEN mcd.column_simple_type = 'date'
            THEN
              jsonb_build_object('days', mtp.default_date_noise_days)
          END
        )
      WHEN mcr.operation IN ('random', 'random_lut')
        THEN (
          CASE
          WHEN mcd.column_simple_type = 'int2'
            THEN
              jsonb_build_object('range_start', -32768, 'range_end', 32767)
          WHEN mcd.column_simple_type = 'int4'
            THEN
              jsonb_build_object('range_start', -2147483648, 'range_end', 2147483647)
          WHEN mcd.column_simple_type = 'int8'
            THEN
              jsonb_build_object('range_start', -9223372036854775808, 'range_end', 9223372036854775807)
          WHEN mcd.column_simple_type = 'float4'
            THEN
              jsonb_build_object('range_start', -16777216, 'range_end', 16777216)
          WHEN mcd.column_simple_type = 'float8'
            THEN
              jsonb_build_object('range_start', -9007199254740991, 'range_end', 9007199254740991)
          END
        )
      END, '{}' :: JSONB) args,
    mcd.column_simple_type,
    mcd.column_type,
    mcd.column_position
  FROM themask.table_policy mtp
    JOIN themask._col_defs mcd USING (schema_name, table_name)
    LEFT JOIN themask.column_rule mcr USING (context_name, schema_name, table_name, column_name);
