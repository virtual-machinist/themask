CREATE FUNCTION themask.add_table_policy(_context_name           TEXT, _schema_name TEXT, _table_name TEXT,
                                         column_copy_mode        themask.MASK_DEFAULT_COLUMN_MODE DEFAULT 'dont',
                                         numeric_noise_fraction  DOUBLE PRECISION DEFAULT 0.1,
                                         date_noise_days         INT DEFAULT 5,
                                         timestamp_noise_seconds DOUBLE PRECISION DEFAULT 3600
)
  RETURNS VOID AS
$$
BEGIN
  ASSERT NOT themask._check_table_policy_exists(_context_name, _schema_name, _table_name),
  'table policy already exists';
  ASSERT themask._obj_exists(_schema_name, _table_name), 'specified table does not exist';

  RAISE NOTICE 'adding %.% table policy for masking context %', quote_ident(_schema_name), quote_ident(_table_name),
  _context_name;

  INSERT INTO themask.table_policy (context_name, schema_name, table_name, default_column_copy_mode,
                                    default_numeric_noise_fraction, default_date_noise_days,
                                    default_timestamp_noise_seconds)
  VALUES (_context_name, _schema_name, _table_name, column_copy_mode, numeric_noise_fraction, date_noise_days,
          timestamp_noise_seconds);
END;
$$
VOLATILE
LANGUAGE plpgsql;
