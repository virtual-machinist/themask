CREATE FUNCTION themask.run(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _start_time         TIMESTAMP;
  _compiled_count     BIGINT;
  _rule_count         BIGINT;
  _compiled_operation themask.COMPILED_OPERATION;
BEGIN
  RAISE NOTICE 'running masking operations for masking context %', _context_name;
  _start_time := clock_timestamp();

  SELECT count(*)
  INTO _compiled_count
  FROM themask.compiled_operation
  WHERE context_name = _context_name;
  IF _compiled_count = 0
  THEN
    SELECT count(*)
    INTO _rule_count
    FROM themask._default_operations
    WHERE context_name = _context_name;
    IF _rule_count > 0
    THEN
      RAISE NOTICE 'no compiled rules found, performing implicit compilation';
      PERFORM themask.compile_rules(_context_name);
    ELSE
      RAISE WARNING 'no rules defined for masking context %', _context_name;
      RETURN;
    END IF;
  END IF;

  FOR _compiled_operation IN SELECT *
                             FROM themask.compiled_operation
                             WHERE context_name = _context_name
                             ORDER BY operation_order
  LOOP
    RAISE DEBUG 'running operation %', _compiled_operation.operation_order;
    EXECUTE _compiled_operation.operation_sql;
  END LOOP;

  RAISE NOTICE 'masking operations completed successfully in %', clock_timestamp() - _start_time;
END;
$$
VOLATILE
STRICT
LANGUAGE plpgsql;
