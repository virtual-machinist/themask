CREATE FUNCTION themask.add_column_rule(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT,
                                        _operation    themask.MASK_OPERATION DEFAULT NULL, _args JSONB DEFAULT NULL
)
  RETURNS VOID AS
$$
DECLARE
  _default_column_mode themask.MASK_DEFAULT_COLUMN_MODE;
  _mask_operation      themask.MASK_OPERATION := _operation;
BEGIN
  ASSERT themask._obj_exists(_schema_name, _table_name, _column_name), 'column does not exist';
  ASSERT NOT themask._check_column_rule_exists(_context_name, _schema_name, _table_name, _column_name),
  'rule for the column already exists';

  PERFORM themask._get_context(_context_name);

  RAISE NOTICE 'adding %.%.% column rule for masking context %', quote_ident(_schema_name), quote_ident(_table_name),
  quote_ident(_column_name), _context_name;

  IF NOT themask._check_table_policy_exists(_context_name, _schema_name, _table_name)
  THEN
    RAISE NOTICE 'table policy does not exist, creating one implicitly';
    PERFORM themask.add_table_policy(_context_name, _schema_name, _table_name);
  END IF;

  IF _mask_operation IS NULL
  THEN
    SELECT default_column_copy_mode
    INTO _default_column_mode
    FROM themask.table_policy
    WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name;

    IF _default_column_mode = 'as_is'
    THEN
      _mask_operation := 'copy';
    ELSE
      _mask_operation := 'nullify';
    END IF;
  END IF;

  PERFORM themask._check_operation_supported(_schema_name, _table_name, _column_name, _mask_operation);

  INSERT INTO themask.column_rule (context_name, schema_name, table_name, column_name, args, operation)
  VALUES (_context_name, _schema_name, _table_name, _column_name, coalesce(_args, '{}' :: JSONB), _mask_operation);
END;
$$
VOLATILE
LANGUAGE plpgsql;
