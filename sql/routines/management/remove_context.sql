CREATE FUNCTION themask.remove_context(_context_name TEXT)
  RETURNS VOID AS
$$
BEGIN
  PERFORM themask._get_context(_context_name); -- check if exists

  RAISE NOTICE 'removing masking context % including configuration and compiled rules', _context_name;

  DELETE FROM themask.compiled_operation
  WHERE context_name = _context_name;
  DELETE FROM themask.column_rule
  WHERE context_name = _context_name;
  DELETE FROM themask.table_policy
  WHERE context_name = _context_name;
  DELETE FROM themask.mask_context
  WHERE name = _context_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
