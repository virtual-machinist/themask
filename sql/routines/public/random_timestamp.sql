CREATE FUNCTION themask.random_timestamp(range_start TIMESTAMP WITH TIME ZONE, range_end TIMESTAMP WITH TIME ZONE)
  RETURNS TIMESTAMP WITH TIME ZONE AS
$$
SELECT range_start + age(range_end, range_start) * random();
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_timestamp()
  RETURNS TIMESTAMP WITH TIME ZONE AS
$$
SELECT themask.random_timestamp(to_timestamp(0), current_timestamp);
$$
VOLATILE
LANGUAGE SQL;
