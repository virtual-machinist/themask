CREATE FUNCTION themask.random_estonian_pic(gender     VARCHAR(1) DEFAULT themask.random_gender(),
                                            birth_date DATE DEFAULT themask.random_date())
  RETURNS TEXT AS
$$
SELECT themask.estonian_pic(gender, birth_date, themask.random_number(1, 1000))
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_estonian_pic(gender VARCHAR(1), birth_date_start DATE, birt_date_end DATE)
  RETURNS TEXT AS
$$
SELECT themask.random_estonian_pic(gender, themask.random_date(birth_date_start, birt_date_end));
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;
