CREATE FUNCTION themask._estonian_pic_glue_parts(prefix INT, birth_date INT, order_number INT)
  RETURNS BIGINT AS
$$
SELECT prefix :: BIGINT * (10 ^ 9) :: BIGINT + birth_date :: BIGINT * 1000 + order_number;
$$
IMMUTABLE
STRICT
LANGUAGE SQL;
