CREATE FUNCTION themask.random_name(ntype VARCHAR(1))
  RETURNS TEXT AS
$$
WITH randomized AS (
  (SELECT (greatest((max(n.rank) * random()) :: INT, 1)) AS rank
   FROM themask.popular_name n
   WHERE n.name_type = ntype)
)
SELECT pn.name
FROM themask.popular_name pn, randomized
WHERE pn.name_type = ntype AND pn.rank = randomized.rank
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;
