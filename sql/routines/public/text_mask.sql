CREATE FUNCTION themask.text_mask(string TEXT, left_offset INT, right_offset INT, mask_char VARCHAR(1) DEFAULT 'X')
  RETURNS TEXT AS
$$
WITH str AS (
  SELECT character_length(string) AS len
), idx AS (
  SELECT
    least(left_offset, len)                                           AS l,
    least(greatest(len - right_offset, least(left_offset, len)), len) AS r
  FROM str
)
SELECT overlay(string PLACING repeat(mask_char, r - l) FROM l + 1)
FROM idx;
$$
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
