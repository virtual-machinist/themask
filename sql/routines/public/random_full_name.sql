CREATE FUNCTION themask.random_full_name(VARIADIC fmt TEXT [])
  RETURNS TEXT AS
$$
-- TODO ugly?
SELECT string_agg(converted.name, '')
FROM (
       SELECT CASE
              WHEN u = 'Ff'
                THEN initcap(themask.random_name('F'))
              WHEN u = 'FF'
                THEN upper(themask.random_name('F'))
              WHEN u = 'ff'
                THEN lower(themask.random_name('F'))
              WHEN u = 'Mf'
                THEN initcap(themask.random_name('M'))
              WHEN u = 'MF'
                THEN upper(themask.random_name('M'))
              WHEN u = 'mf'
                THEN lower(themask.random_name('M'))
              WHEN u = 'Ln'
                THEN initcap(themask.random_name('L'))
              WHEN u = 'LN'
                THEN upper(themask.random_name('L'))
              WHEN u = 'ln'
                THEN lower(themask.random_name('L'))
              WHEN u = 'Rf'
                THEN initcap(themask.random_name(upper(themask.random_gender())))
              WHEN u = 'RF'
                THEN upper(themask.random_name(upper(themask.random_gender())))
              WHEN u = 'rf'
                THEN lower(themask.random_name(upper(themask.random_gender())))
              ELSE u END AS name
       FROM unnest(fmt) u
     ) converted;
$$
VOLATILE
LANGUAGE SQL;


CREATE FUNCTION themask.random_full_name(gender varchar(1))
  RETURNS TEXT AS
$$
SELECT themask.random_full_name(concat(upper(gender), 'f'), ' ', 'Ln');
$$
VOLATILE
LANGUAGE SQL;


CREATE FUNCTION themask.random_full_name()
  RETURNS TEXT AS
$$
SELECT themask.random_full_name('Rf', ' ', 'Ln');
$$
VOLATILE
LANGUAGE SQL;
