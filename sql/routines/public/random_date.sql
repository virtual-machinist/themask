CREATE FUNCTION themask.random_date(range_start DATE, range_end DATE)
  RETURNS DATE AS
$$
SELECT themask.random_timestamp(range_start :: TIMESTAMP WITH TIME ZONE, range_end :: TIMESTAMP WITH TIME ZONE) :: DATE;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_date()
  RETURNS DATE AS
$$
SELECT themask.random_timestamp() :: DATE;
$$
VOLATILE
LANGUAGE SQL;
