CREATE FUNCTION themask._check_table_policy_exists(_context_name TEXT, _schema_name TEXT, _table_name TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) > 0
FROM themask.table_policy
WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name;
$$
STABLE
LANGUAGE SQL;
