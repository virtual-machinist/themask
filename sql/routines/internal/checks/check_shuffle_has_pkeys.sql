-- may not be needed after another shuffling method is implemented, that does not involve direct PK usage
CREATE FUNCTION themask._check_shuffle_has_pkeys(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _count BIGINT;
BEGIN
  SELECT count(*)
  INTO _count
  FROM themask.table_policy mtp
    JOIN themask.column_rule mcr USING (context_name, schema_name, table_name)
  WHERE mtp.context_name = _context_name AND mcr.operation = 'shuffle' AND NOT
  exists(SELECT 1
         FROM themask._pkeys mpk
         WHERE mpk.schema_name = mtp.schema_name AND mpk.table_name = mtp.table_name
  );

  IF _count > 0
  THEN
    RAISE EXCEPTION 'tables without a primary key are not supported with shuffle masking method, % found', _count;
  END IF;
END;
$$
STABLE
LANGUAGE plpgsql;
