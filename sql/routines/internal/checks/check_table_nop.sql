CREATE FUNCTION themask._check_table_nop(_context_name TEXT, _schema_name TEXT, _table_name TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) = 0
FROM themask._default_operations
WHERE context_name = _context_name AND schema_name = _schema_name
      AND table_name = _table_name AND operation <> 'nullify';
$$
STABLE
LANGUAGE SQL;
