CREATE FUNCTION themask._check_operation_supported(_schema_name TEXT, _table_name TEXT, _column_name TEXT,
                                                _operation   themask.MASK_OPERATION)
  RETURNS VOID AS
$$
DECLARE
  _column_type TEXT;
  _supported   BOOLEAN := FALSE;
BEGIN
  SELECT column_simple_type
  INTO STRICT _column_type
  FROM themask._col_defs
  WHERE schema_name = _schema_name AND table_name = _table_name AND column_name = _column_name;

  -- TODO check args
  CASE
    WHEN _operation = 'noise'
    THEN _supported = _column_type IN
                      ('int2', 'int4', 'int8', 'float4', 'float8', 'numeric', 'date', 'timestamp', 'timestamptz');
    WHEN _operation IN ('random', 'truncate', 'random_lut')
    THEN _supported = _column_type IN
                      ('int2', 'int4', 'int8', 'float4', 'float8', 'numeric', 'date', 'timestamp', 'timestamptz',
                               'varchar', 'text');
    WHEN _operation = 'mask'
    THEN _supported = _column_type IN ('varchar', 'text');
    WHEN _operation = 'shuffle'
    THEN
      SELECT NOT exists(
        SELECT
          count(*),
          schema_name,
          table_name
        FROM themask._pkeys
        GROUP BY schema_name, table_name
        HAVING count(*) > 1 AND schema_name = _schema_name AND table_name = _table_name
      )
      INTO _supported;
  ELSE
    _supported := TRUE;
  END CASE;

  ASSERT _supported, 'masking operation not supported for this column type';
END;
$$
STABLE
LANGUAGE plpgsql;
