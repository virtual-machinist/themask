CREATE FUNCTION themask._check_column_rule_exists(_context_name TEXT, _schema_name TEXT, _table_name TEXT,
                                                 _column_name  TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) > 0
FROM themask.column_rule
WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name AND
      column_name = _column_name;
$$
STABLE
LANGUAGE SQL;
