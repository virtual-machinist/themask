CREATE FUNCTION themask._get_mandatory_column_counts(_context_name TEXT)
  RETURNS TABLE(schema_name TEXT, table_name TEXT, mandatory_count BIGINT, declared_count BIGINT) AS
$$
WITH mandatory AS (
  SELECT
    count(*) AS column_count,
    mcd.schema_name,
    mcd.table_name
  FROM themask._col_defs mcd
    JOIN themask.table_policy mtp USING (schema_name, table_name)
  WHERE mtp.default_column_copy_mode = 'fail' AND mtp.context_name = _context_name
  GROUP BY mcd.schema_name, mcd.table_name
), declared AS (
  SELECT
    count(*) AS column_count,
    mcr.schema_name,
    mcr.table_name
  FROM themask.column_rule mcr
    JOIN themask._col_defs mcd USING (schema_name, table_name, column_name)
  WHERE mcr.context_name = _context_name
  GROUP BY mcr.schema_name, mcr.table_name
)
SELECT
  m.schema_name,
  m.table_name,
  m.column_count              AS mandatory_count,
  coalesce(d.column_count, 0) AS declared_count
FROM mandatory m LEFT JOIN declared d USING (schema_name, table_name)
$$
LANGUAGE SQL
STABLE;
