CREATE FUNCTION themask._get_lut_name(_context     themask.MASK_CONTEXT, _schema_name TEXT, _table_name TEXT,
                                     _column_name TEXT)
  RETURNS TEXT AS
$$
SELECT _schema_name || '_' || _table_name || '_' || _column_name || _context.lut_suffix;
$$
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;


CREATE FUNCTION themask._get_lut_name(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS TEXT AS
$$
SELECT themask._get_lut_name(context, _schema_name, _table_name, _column_name)
FROM themask.mask_context context
WHERE context.name = _context_name;
$$
LANGUAGE SQL
STABLE
RETURNS NULL ON NULL INPUT;
