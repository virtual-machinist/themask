CREATE FUNCTION themask._get_column_type_def(_schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS TEXT AS
$$
DECLARE
  _column_type TEXT;
BEGIN
  SELECT d.column_type
  INTO _column_type
  FROM themask._col_defs d
  WHERE d.schema_name = _schema_name AND d.table_name = _table_name AND d.column_name = _column_name;

  IF NOT FOUND OR _column_type IS NULL
  THEN
    RAISE EXCEPTION 'cannot get type definition for %.%.%', quote_ident(_schema_name), quote_ident(
      _table_name), quote_ident(_column_name);
  END IF;

  RETURN _column_type;
END;
$$
STABLE
LANGUAGE plpgsql;
