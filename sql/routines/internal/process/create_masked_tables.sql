CREATE FUNCTION themask._create_masked_tables(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _trunc_sql CONSTANT         TEXT := 'TRUNCATE %I.%I CASCADE';
  _sql CONSTANT               TEXT := 'CREATE %1$s TABLE %2$I.%3$I (LIKE %4$I.%3$I INCLUDING COMMENTS)';
  _drop_not_null_sql CONSTANT TEXT := 'ALTER TABLE %1$I.%2$I ALTER COLUMN %3$I DROP NOT NULL';
  _context                    themask.MASK_CONTEXT;
  _table_policy               themask.TABLE_POLICY;
  _schema_name                TEXT;
  _table_name                 TEXT;
  _masked_schema              TEXT;
  _unlogged                   TEXT := '';
  _not_null_column            TEXT;
BEGIN
  RAISE NOTICE 'creating masked tables for context %', _context_name;

  _context := themask._get_context(_context_name);

  IF _context.make_unlogged_tables
  THEN
    _unlogged := 'UNLOGGED';
  END IF;

  FOR _table_policy IN SELECT *
                       FROM themask.table_policy
                       WHERE context_name = _context_name
                       ORDER BY schema_name, table_name
  LOOP
    BEGIN
      _schema_name := _table_policy.schema_name;
      _table_name := _table_policy.table_name;

      IF NOT themask._obj_exists(_schema_name, _table_name)
      THEN
        RAISE WARNING 'source table %.% does not exist, no masked table will be created', quote_ident(
          _schema_name), quote_ident(_table_name);
        CONTINUE;
      END IF;

      _masked_schema := themask._get_masked_schema_name(_context, _schema_name);

      IF themask._obj_exists(_masked_schema, _table_name)
      THEN
        IF _context.truncate_existing_masked_tables
        THEN
          RAISE NOTICE 'truncating masked table %.% with all dependents', quote_ident(_masked_schema),
          quote_ident(_table_name);
          EXECUTE format(_trunc_sql, _masked_schema, _table_name);
          CONTINUE;
        END IF;

        RAISE EXCEPTION 'masked table %.% already exists', quote_ident(_masked_schema), quote_ident(_table_name);
      END IF;

      EXECUTE format(_sql, _unlogged, _masked_schema, _table_name, _schema_name);

      FOR _not_null_column IN SELECT column_name
                              FROM information_schema.columns
                              WHERE table_schema = _masked_schema AND table_name = _table_name AND is_nullable = 'NO'
      LOOP
        RAISE NOTICE 'dropping NOT NULL for column %', quote_ident(_not_null_column);
        EXECUTE format(_drop_not_null_sql, _masked_schema, _table_name, _not_null_column);
      END LOOP;
    END;
  END LOOP;

  IF NOT FOUND
  THEN
    RAISE EXCEPTION 'no table rules configured for masking context %', _context_name;
  END IF;
END;
$$
VOLATILE
LANGUAGE plpgsql;
