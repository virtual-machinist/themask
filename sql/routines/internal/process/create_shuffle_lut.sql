CREATE FUNCTION themask._create_shuffle_lut(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS BOOLEAN AS
$$
DECLARE
  _context     themask.MASK_CONTEXT%ROWTYPE;
  _pkey_type   TEXT;
  _column_type TEXT;
  _lut_name    TEXT;
BEGIN
  RAISE NOTICE 'creating shuffle LUT for %.%.%', quote_ident(_schema_name), quote_ident(_table_name),
  quote_ident(_column_name);

  _context := themask._get_context(_context_name);

  SELECT themask._get_column_type_def(_schema_name, _table_name, p.pkey_column)
  INTO STRICT _pkey_type
  FROM (SELECT pkey_column
        FROM themask._pkeys
        WHERE schema_name = _schema_name AND table_name = _table_name) p;

  ASSERT _pkey_type IS NOT NULL, 'unable to determine primary key type';

  _column_type := themask._get_column_type_def(_schema_name, _table_name, _column_name);
  _lut_name := quote_ident(themask._get_lut_name(_context, _schema_name, _table_name, _column_name));

  RETURN themask._create_lut(_context, _lut_name, _pkey_type, _column_type);
END;
$$
VOLATILE
LANGUAGE plpgsql;
