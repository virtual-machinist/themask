CREATE FUNCTION themask._create_lut(_context     themask.MASK_CONTEXT, _lut_name TEXT, _original_type TEXT,
                                    _masked_type TEXT, _unique_records BOOLEAN DEFAULT FALSE)
  RETURNS BOOLEAN AS
$$
DECLARE
  _sql_composite CONSTANT TEXT := 'CREATE %1$s TABLE %2$I (original %3$s NOT NULL, masked %4$s NOT NULL,' ||
                                  ' PRIMARY KEY (original, masked)) %5$s';
  _sql                    TEXT := 'CREATE %1$s TABLE %2$I (original %3$s NOT NULL, masked %4$s,' ||
                                  ' PRIMARY KEY (original)) %5$s';
  _modifier               TEXT := 'TEMP';
  _on_commit_drop         TEXT := 'ON COMMIT DROP';
BEGIN
  IF NOT _context.make_temporary_luts
  THEN
    _modifier := '';
    _on_commit_drop := '';
  ELSEIF _context.make_unlogged_luts
    THEN
      _modifier := 'UNLOGGED';
  END IF;

  IF (_unique_records)
  THEN
    _sql := _sql_composite;
  END IF;

  EXECUTE format(_sql, _modifier, _lut_name, _original_type, _masked_type, _on_commit_drop);
  RETURN TRUE;

  EXCEPTION WHEN DUPLICATE_TABLE
  THEN
    BEGIN
      RAISE NOTICE 'LUT % already exists', quote_ident(_lut_name);
      RETURN FALSE;
    END;
END;
$$
VOLATILE
LANGUAGE plpgsql;


CREATE FUNCTION themask._create_lut(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS BOOLEAN AS
$$
DECLARE
  _context     themask.MASK_CONTEXT%ROWTYPE;
  _column_type TEXT;
  _lut_name    TEXT;
BEGIN
  RAISE NOTICE 'creating LUT for %.%.%', quote_ident(_schema_name), quote_ident(_table_name), quote_ident(_column_name);

  _context := themask._get_context(_context_name);
  _column_type := themask._get_column_type_def(_schema_name, _table_name, _column_name);
  _lut_name := themask._get_lut_name(_context, _schema_name, _table_name, _column_name);

  RETURN themask._create_lut(_context, _lut_name, _column_type, _column_type);
END;
$$
VOLATILE
LANGUAGE plpgsql;
