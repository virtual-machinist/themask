CREATE FUNCTION themask._create_masked_schemas(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _sql CONSTANT       TEXT := 'CREATE SCHEMA %I';
  _schema_name        TEXT;
  _masked_schema_name TEXT;
BEGIN
  RAISE NOTICE 'creating masked schemas for context %', _context_name;

  FOR _schema_name IN SELECT DISTINCT schema_name
                      FROM themask.table_policy
                      WHERE context_name = _context_name
                      ORDER BY schema_name
  LOOP
    BEGIN
      IF NOT themask._obj_exists(_schema_name)
      THEN
        RAISE WARNING 'source schema % does not exist, no masked schema will be created', quote_ident(_schema_name);
        CONTINUE;
      END IF;

      _masked_schema_name := themask._get_masked_schema_name(_context_name, _schema_name);

      EXECUTE format(_sql, _masked_schema_name);

      EXCEPTION WHEN DUPLICATE_SCHEMA
      THEN
        RAISE NOTICE 'masked schema % already exists', quote_ident(_masked_schema_name);
    END;
  END LOOP;

  IF NOT FOUND
  THEN
    RAISE EXCEPTION 'no table rules configured for masking context %', _context_name;
  END IF;
END;
$$
VOLATILE
LANGUAGE plpgsql;
