CREATE FUNCTION themask._get_mask_populate_function(_column_name TEXT, _simple_type TEXT, _args JSONB,
                                                    _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
DECLARE
  _function     TEXT;
  _left_offset  INT = 0;
  _right_offset INT = 0;
  _mask_char    TEXT;
BEGIN
  IF _simple_type NOT IN ('varchar', 'text')
  THEN
    RETURN NULL;
  END IF;

  IF jsonb_typeof(_args -> 'left_offset') = 'number'
  THEN
    _left_offset := ((_args ->> 'left_offset') :: NUMERIC) :: INT;
  END IF;

  IF jsonb_typeof(_args -> 'right_offset') = 'number'
  THEN
    _right_offset := ((_args ->> 'right_offset') :: NUMERIC) :: INT;
  END IF;

  IF _left_offset > 0 OR _right_offset > 0
  THEN
    _mask_char := left(coalesce(_args ->> 'mask_char', 'X'), 1);
    RETURN format('themask.text_mask(%s.%I, %s, %s, %L)', _table_alias, _column_name, _left_offset, _right_offset,
                  _mask_char);
  END IF;

  RETURN _function;
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
