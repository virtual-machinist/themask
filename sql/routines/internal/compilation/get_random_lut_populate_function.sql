CREATE FUNCTION themask._get_random_lut_populate_function(_context_name TEXT, _schema_name TEXT, _table_name TEXT,
                                                          _column_name  TEXT, _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
BEGIN
  -- should be prefilled by INSERT time
  RETURN themask._get_lut_populate_function(
    themask._get_lut_name(_context_name, _schema_name, _table_name, _column_name),
    _column_name, _table_alias);
END;
$$
STRICT
STABLE
LANGUAGE plpgsql;
