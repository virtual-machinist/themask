CREATE FUNCTION themask._add_independent_shuffle_columns(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _shuffle_add_sql CONSTANT      TEXT := 'SELECT themask._create_shuffle_lut(%L, %L, %L, %L)';
  _shuffle_populate_sql CONSTANT TEXT := 'SELECT themask._populate_shuffle_lut(%L, %L, %L, %L)';
  _operations                    themask._DEFAULT_OPERATIONS;
BEGIN
  FOR _operations IN
  SELECT *
  FROM themask._default_operations
  WHERE context_name = _context_name AND NOT args ?& ARRAY ['schema', 'table', 'column'] AND operation = 'shuffle'
  LOOP
    PERFORM themask._add_compiled_operation(_context_name, format(_shuffle_add_sql, _context_name,
                                                                  _operations.schema_name, _operations.table_name,
                                                                  _operations.column_name));
    PERFORM themask._add_compiled_operation(_context_name, format(_shuffle_populate_sql, _context_name,
                                                                  _operations.schema_name, _operations.table_name,
                                                                  _operations.column_name));
  END LOOP;
END;
$$
VOLATILE
LANGUAGE plpgsql;
