CREATE FUNCTION themask._get_noise_populate_function(_column_name TEXT, _simple_type TEXT, _args JSONB,
                                                     _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
DECLARE
  _function TEXT;
BEGIN
  CASE
    WHEN _simple_type IN ('int8', 'int4', 'int2', 'float4', 'float8', 'numeric')
         AND jsonb_typeof(_args -> 'fraction') = 'number'
    THEN
      _function := format('themask.random_noise(%s.%I, %s)', _table_alias, _column_name, _args ->> 'fraction');
    WHEN _simple_type IN ('timestamp', 'timestamptz') AND jsonb_typeof(_args -> 'seconds') = 'number'
    THEN
      _function := format('themask.random_noise(%s.%I, %s)', _table_alias, _column_name, _args ->> 'seconds');
    WHEN _simple_type = 'date' AND jsonb_typeof(_args -> 'days') = 'number'
    THEN
      _function := format('themask.random_noise(%s.%I, %s::INT)', _table_alias, _column_name, _args ->> 'days');
  END CASE;

  RETURN _function;
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
