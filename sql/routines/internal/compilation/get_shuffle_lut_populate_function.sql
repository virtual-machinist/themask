CREATE FUNCTION themask._get_shuffle_lut_populate_function(_context_name TEXT, _schema_name TEXT, _table_name TEXT,
                                                           _column_name  TEXT, _args JSONB,
                                                           _table_alias  TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
DECLARE
  _lut_schema      TEXT;
  _lut_table       TEXT;
  _lut_column      TEXT;
  _original_column TEXT;
  _lut_name        TEXT;
BEGIN
  IF jsonb_typeof(_args -> 'other_schema') = 'string' AND jsonb_typeof(_args -> 'other_table') = 'string'
     AND jsonb_typeof(_args -> 'other_column') = 'string' AND jsonb_typeof(_args -> 'fkey_column') = 'string'
  THEN
    _lut_schema := _args ->> 'other_schema';
    _lut_table := _args ->> 'other_table';
    _lut_column := _args ->> 'other_column';
    _original_column := _args ->> 'fkey_column';

    ASSERT themask._obj_exists(_lut_schema, _lut_table, _lut_column), 'Source column for shuffle LUT does not exist';
  ELSE
    _lut_schema := _schema_name;
    _lut_table := _table_name;
    _lut_column := _column_name;

    SELECT pkey_column
    INTO STRICT _original_column
    FROM themask._pkeys
    WHERE schema_name = _lut_schema AND table_name = _lut_table;
  END IF;

  _lut_name := themask._get_lut_name(_context_name, _lut_schema, _lut_table, _lut_column);
  ASSERT _lut_table IS NOT NULL, 'LUT name cannot be NULL';

  RETURN themask._get_lut_populate_function(_lut_name, _original_column, _table_alias);
END;
$$
STRICT
STABLE
LANGUAGE plpgsql;
