CREATE FUNCTION themask._get_copy_populate_function(_column_name TEXT, _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
BEGIN
  RETURN format('%s.%I', _table_alias, _column_name);
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
