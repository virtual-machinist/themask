CREATE FUNCTION themask._get_truncate_populate_function(_column_name TEXT, _simple_type TEXT, _args JSONB,
                                                        _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
DECLARE
  _date_parts CONSTANT TEXT [] = ARRAY ['microseconds', 'milliseconds', 'second', 'minute', 'hour', 'day',
  'week', 'month', 'quarter', 'year', 'decade', 'century', 'millennium'];
  _function            TEXT;
  _text_direction      TEXT;
BEGIN
  CASE
    WHEN _simple_type IN ('int8', 'int4', 'int2', 'float4', 'float8', 'numeric')
         AND jsonb_typeof(_args -> 'digits') = 'number'
    THEN
      _function := format('trunc(%s.%I, %s)', _table_alias, _column_name, _args ->> 'digits');
    WHEN _simple_type IN ('date', 'timestamp', 'timestamptz') AND _args ->> 'precision' = ANY (_date_parts)
    THEN
      _function := format('date_trunc(%L, %s.%I)', _args ->> 'precision', _table_alias, _column_name);
    WHEN _simple_type IN ('varchar', 'text') AND jsonb_typeof(_args -> 'length') = 'number'
    THEN
      -- intentionally swapped - in this context direction means where the symbols are trimmed from
      IF lower(coalesce(_args ->> 'direction', 'right')) = 'right'
      THEN
        _text_direction := 'left';
      ELSEIF lower(_args ->> 'direction') = 'left'
        THEN
          _text_direction := 'right';
      END IF;

      IF _text_direction IS NOT NULL
      THEN
        _function := format('%s(%s.%I, %s::INT)', _text_direction, _table_alias, _column_name, _args ->> 'length');
      END IF;
  END CASE;

  RETURN _function;
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
