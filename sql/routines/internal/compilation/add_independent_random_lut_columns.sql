CREATE FUNCTION themask._add_independent_random_lut_columns(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _lut_add_sql CONSTANT      TEXT := 'SELECT themask._create_lut(%L, %L, %L, %L)';
  _lut_populate_sql CONSTANT TEXT := 'SELECT themask._populate_lut(%L, %L, %L, %L, %L)';
  _lut_populate_function     TEXT;
  _operations                themask._DEFAULT_OPERATIONS;
BEGIN
  FOR _operations IN
  SELECT *
  FROM themask._default_operations
  WHERE context_name = _context_name AND NOT args ?& ARRAY ['schema', 'table', 'column'] AND operation = 'random_lut'
  LOOP
    PERFORM themask._add_compiled_operation(_context_name, format(_lut_add_sql, _context_name, _operations.schema_name,
                                                                  _operations.table_name, _operations.column_name));
    _lut_populate_function = themask._get_random_populate_function(_operations.column_simple_type, _operations.args);

    IF _lut_populate_function IS NULL
    THEN
      RAISE EXCEPTION 'cannot handle random_lut for %.%.%', quote_ident(_operations.schema_name),
      quote_ident(_operations.table_name), quote_ident(_operations.column_name)
      USING HINT = 'not enough valid arguments?';
    END IF;

    PERFORM themask._add_compiled_operation(_context_name, format(_lut_populate_sql, _context_name,
                                                                  _operations.schema_name, _operations.table_name,
                                                                  _operations.column_name, _lut_populate_function));
  END LOOP;
END;
$$
VOLATILE
LANGUAGE plpgsql;
