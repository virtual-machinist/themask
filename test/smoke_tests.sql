--basic smoke tests
--todo add asserts

SELECT random_timestamp();
SELECT random_date();

SELECT random_number(1 :: DOUBLE PRECISION, 10 :: DOUBLE PRECISION);
SELECT random_number(1 :: REAL, 10 :: REAL);
SELECT random_number(1 :: NUMERIC, 10 :: NUMERIC);
SELECT random_number(1 :: SMALLINT, 10 :: SMALLINT);
SELECT random_number(1 :: INT, 10 :: INT);
SELECT random_number(1 :: BIGINT, 10 :: BIGINT);

SELECT random_boolean();
SELECT random_signum();

SELECT random_noise('1970-01-01' :: DATE, 10);
SELECT random_noise('1970-01-01' :: TIMESTAMP, 86400);
SELECT random_noise(10000 :: SMALLINT, 0.1);
SELECT random_noise(100000 :: INT, 0.1);
SELECT random_noise(10000000000 :: BIGINT, 0.1);
SELECT random_noise(19223372036854775807 :: NUMERIC, 0.1);

SELECT random_name('F');
SELECT random_name('M');
SELECT random_name('L');
SELECT random_name(NULL);

SELECT _estonian_pic_checksum(3871220022);
SELECT estonian_pic_checksum(3, 871220, 22);
SELECT estonian_pic('M', '1987-12-20'::date, 22);

select random_estonian_pic('M', '1987-12-20'::date);
select random_estonian_pic(gender :=  'M');
select random_estonian_pic(birth_date := '1987-12-20'::date);
select random_estonian_pic();

SELECT text_mask('1234567890', 5, 0, '*');
SELECT text_mask('1234567890', 0, 5);
SELECT text_mask('1234567890', 2, 2, '_');

select _get_column_alias('public', 'test_names', 'name');

select _get_mask_populate_function('col', 'varchar', '{"left_offset": 5}'::JSONB);
select _get_shuffle_lut_populate_function('test', 'public', 'test_names', 'name', '{}'::jsonb);

create table ttt (
  id bigint PRIMARY KEY,
  test_id BIGINT REFERENCES test_names(id)
);
select _mask_get_shuffle_lut_populate_function('test', 'public', 'ttt', 'test_id',
   '{"other_schema": "public", "other_table": "test_table", "other_column": "id", "fkey_column": "test_id"}'::jsonb);
drop TABLE ttt;

select _get_copy_populate_function('col');
select _get_nullify_populate_function();

select _get_truncate_populate_function('col', 'date', '{"precision": "milliseconds"}'::JSONB);
select _get_random_populate_function('varchar', '{"type": "name"}'::jsonb);
select _get_random_populate_function('varchar', '{"type": "name", "args": "''Rf'', '' '', ''Rl''"}'::jsonb);
select _get_random_populate_function('varchar', '{"type": "estonian_pic"}'::jsonb);

select random_full_name('Ff');

select _check_table_nop('text', 'public', 'test_names');

select add_context('test2');
select modify_context('test2', suffix_for_luts := '_lut');
select remove_context('test2');
select add_table_policy('test', 'public', 'test_table');
select modify_table_policy('test', 'public', 'test_table');
select modify_table_policy('test', 'public', 'test_table', column_copy_mode := 'as_is');
select remove_table_policy('test', 'public', 'test_table');
select add_column_rule('test', 'public', 'test_table', 'str_varchar', 'mask');
-- select mask_add_column_rule('test', 'public', 'test_table', 'id', 'mask');
