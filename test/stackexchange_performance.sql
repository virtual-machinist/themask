SELECT themask.add_context('test');
SELECT themask.add_table_policy('test', 'public', 'comments', 'as_is');
SELECT themask.add_table_policy('test', 'public', 'posts', 'as_is');
SELECT themask.add_table_policy('test', 'public', 'users', 'as_is');
-- operation being tested goes here
SELECT themask.compile_rules('test');

DO $$
DECLARE
  run_start TIMESTAMP;
  constraint_start TIMESTAMP;
  run_end TIMESTAMP;
BEGIN
  run_start := clock_timestamp();
  PERFORM themask.run('test');
  constraint_start := clock_timestamp();

  SET SEARCH_PATH = "fake_public";

  ALTER TABLE comments add CONSTRAINT comments_pkey PRIMARY KEY (id);
  ALTER TABLE comments ALTER COLUMN postid SET NOT NULL ;
  ALTER TABLE comments ALTER COLUMN score SET NOT NULL ;
  ALTER TABLE comments ALTER COLUMN creationdate SET NOT NULL ;

  ALTER TABLE posts ADD CONSTRAINT posts_pkey PRIMARY KEY (id);
  ALTER TABLE posts ALTER column posttypeid SET NOT NULL ;
  ALTER TABLE posts ALTER column creationdate SET NOT NULL ;

  ALTER TABLE users ADD CONSTRAINT users_pkey PRIMARY KEY (id);
  ALTER TABLE users ALTER COLUMN reputation SET NOT NULL ;
  ALTER TABLE users ALTER COLUMN creationdate SET NOT NULL ;
  ALTER TABLE users ALTER COLUMN displayname SET NOT NULL ;
  ALTER TABLE users ALTER COLUMN views SET NOT NULL ;
  ALTER TABLE users ALTER COLUMN upvotes SET NOT NULL ;
  ALTER TABLE users ALTER COLUMN downvotes SET NOT NULL ;

  ALTER TABLE comments ADD CONSTRAINT comments_userid_fkey FOREIGN KEY (userid) REFERENCES users(id);
  ALTER TABLE comments ADD CONSTRAINT comments_postid_fkey FOREIGN KEY (postid) REFERENCES posts(id);
  ALTER TABLE posts ADD CONSTRAINT posts_owneruserid_fkey FOREIGN KEY (owneruserid) REFERENCES users(id);
  ALTER TABLE posts ADD CONSTRAINT posts_lasteditoruserid_fkey FOREIGN KEY (lasteditoruserid) REFERENCES users(id);

  -- taken from https://github.com/Networks-Learning/stackexchange-dump-to-postgres
  CREATE INDEX cmnts_score_idx ON Comments USING BTREE (Score) WITH (FILLFACTOR = 100);
  CREATE INDEX cmnts_postid_idx ON Comments USING HASH (PostId) WITH (FILLFACTOR = 100);
  CREATE INDEX cmnts_creation_date_idx ON Comments USING BTREE (CreationDate) WITH (FILLFACTOR = 100);
  CREATE INDEX cmnts_userid_idx ON Comments USING BTREE (UserId) WITH (FILLFACTOR = 100);

  CREATE INDEX posts_post_type_id_idx ON Posts USING BTREE (PostTypeId) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_score_idx ON Posts USING BTREE (Score) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_creation_date_idx ON Posts USING BTREE (CreationDate) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_owner_user_id_idx ON Posts USING HASH (OwnerUserId) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_answer_count_idx ON Posts USING BTREE (AnswerCount) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_comment_count_idx ON Posts USING BTREE (CommentCount) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_favorite_count_idx ON Posts USING BTREE (FavoriteCount) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_viewcount_idx ON Posts USING BTREE (ViewCount) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_accepted_answer_id_idx ON Posts USING BTREE (AcceptedAnswerId) WITH (FILLFACTOR = 100);
  CREATE INDEX posts_parent_id_idx ON Posts USING BTREE (ParentId) WITH (FILLFACTOR = 100);

  CREATE INDEX user_acc_id_idx ON Users USING HASH (AccountId) WITH (FILLFACTOR = 100);
  CREATE INDEX user_display_idx ON Users USING HASH (DisplayName) WITH (FILLFACTOR = 100);
  CREATE INDEX user_up_votes_idx ON Users USING BTREE (UpVotes) WITH (FILLFACTOR = 100);
  CREATE INDEX user_down_votes_idx ON Users USING BTREE (DownVotes) WITH (FILLFACTOR = 100);
  CREATE INDEX user_created_at_idx ON Users USING BTREE (CreationDate) WITH (FILLFACTOR = 100);

  run_end := clock_timestamp();

  RAISE INFO 'mask: %, reindex: %', extract(EPOCH FROM constraint_start - run_start),
  extract(EPOCH FROM run_end - constraint_start);
END;
$$;

DROP SCHEMA IF EXISTS "fake_public" CASCADE ;
VACUUM FULL;
